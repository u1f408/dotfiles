# lauren's dotfiles

this is where my dotfiles live

## usage

this repo uses [emanate] to do symlink management.

```
$ git clone https://gitlab.com/alxce/dotfiles.git $HOME/.dotfiles
$ cd $HOME/.dotfiles
$ pip3 install --user emanate
$ emanate
```

[emanate]: https://github.com/duckinator/emanate

## license

the file `.local/share/nvim/site/autoload/plug.vim` is from [vim-plug],
which is under the MIT license.

the files named `nvm.fish` and `10-nvm.fish` in `.config/fish`
are from [nvm.fish], which is under the MIT license.

the rest of this repository is in the public domain.

[vim-plug]: https://github.com/junegunn/vim-plug
[nvm.fish]: https://github.com/jorgebucaran/nvm.fish
