# vim: set ts=4 sw=4 expandtab :

# WSL Xorg hack.
if test -n "$WSL_INTEROP"
    set -gx DISPLAY (awk '/nameserver / {print $2; exit}' /etc/resolv.conf 2>/dev/null):0
    set -gx LIBGL_ALWAYS_INDIRECT 1

    # xfconfd launch
    if test -x /usr/lib64/xfce4/xfconf/xfconfd
        set -gx XFCONF_PID (pgrep -xo xfconfd)
        while test -z $XFCONF_PID
            bash -c "/usr/lib64/xfce4/xfconf/xfconfd --daemon >/dev/null 2>/dev/null & disown"
            sleep 0.5
            set -gx XFCONF_PID (pgrep -xo xfconfd)
        end
    end

    # xfce4-panel launch
    if test -z (pgrep -xo xfce4-panel)
        bash -c "xfce4-panel --disable-wm-check 2>/dev/null >/dev/null & disown"
    end
end

