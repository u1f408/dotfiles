# vim: set ts=4 sw=4 expandtab :

if test -d $HOME/.rvm/bin
    set -gx fish_user_paths $HOME/.rvm/bin $fish_user_paths
end

