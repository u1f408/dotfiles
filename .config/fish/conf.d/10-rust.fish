# vim: set ts=4 sw=4 expandtab :

if test -d $HOME/.cargo/bin
    set -gx fish_user_paths $HOME/.cargo/bin $fish_user_paths
end

