# vim: set ts=4 sw=4 expandtab :

function fish_prompt --description 'Write out the prompt'
    set -l color_host yellow
    set -l color_pwd magenta
    set -l color_virtualenv green
    set -l color_git cyan
    set -l color_user blue
    test "$USER" = "root"; and set -l color_user red

    set -l git_is_repo ''
    set -l git_modified (git status --porcelain ^/dev/null); and set -l git_is_repo true
    if not set -gq __fish_prompt_hostname
        set -gx __fish_prompt_hostname (cat /etc/hostname | cut -d'.' -f1)
    end

    set -l next_color $color_user
    if test -n "$SSH_TTY"
        echo -n -s (set_color -b $color_host white) ' ' "$__fish_prompt_hostname" ' ' (set_color -b $next_color $color_host) ''
    end

    set -l next_color $color_pwd
    test -n "$git_is_repo"; and set -l next_color $color_git
    test -n "$VIRTUAL_ENV"; and set -l next_color $color_virtualenv
    echo -n -s (set_color -b $color_user white) ' ' "$USER" ' ' (set_color -b $next_color $color_user) ''

    set -l next_color $color_pwd
    test -n "$git_is_repo"; and set -l next_color $color_git
    if test -n "$VIRTUAL_ENV"
        set -l virtualenv (basename "$VIRTUAL_ENV" | cut -d'-' -f1)
        echo -n -s (set_color -b $color_virtualenv white) ' ' "🞏" ' ' "$virtualenv" ' ' (set_color -b $next_color $color_virtualenv) ''
    end

    set -l next_color $color_pwd
    if test -n "$git_is_repo"
        set -l git_branch (git symbolic-ref --short HEAD ^/dev/null)
        echo -n -s (set_color -b $color_git white) ' ' '' ' ' "$git_branch" ' '
        if test -n "$git_modified"
            echo -n -s 'M' ' '
        end
        echo -n -s (set_color -b $next_color $color_git) ''
    end

    set -l next_color normal
    echo -n -s (set_color -b $color_pwd white) ' ' (prompt_pwd) ' ' (set_color -b $next_color $color_pwd) ''

    echo -n -s (set_color normal) ' '
end

