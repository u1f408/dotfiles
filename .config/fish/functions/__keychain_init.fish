# vim: set ts=4 sw=4 expandtab :

function __keychain_init
    set -l keychain_keys (for i in $HOME/.ssh/*.pub; basename $i | sed 's|\.pub$||'; end)
    source (keychain --quiet --eval --agents ssh $keychain_keys | psub)
    set -le keychain_keys

    set -gx SSH_AGENT_PID $SSH_AGENT_PID
    set -gx SSH_AUTH_SOCK $SSH_AUTH_SOCK
end

