# vim: set ts=4 sw=4 expandtab :

set -gx EDITOR nvim
set -gx fish_user_paths $HOME/.local/bin

# Bring in conf.d
if test -d $HOME/.config/fish/conf.d
    for file in $HOME/.config/fish/conf.d/*.fish
        source $file
    end
end
