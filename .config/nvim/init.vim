" setup plugins
call plug#begin(stdpath('data') . '/plugged')
Plug 'tpope/vim-sensible'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-fugitive'
Plug 'mhinz/vim-signify'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'junegunn/fzf', { 'do' : { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
call plug#end()

" do some configuration
set background=light
set hidden
set encoding=utf-8
set switchbuf=usetab,newtab

" modelines
set modeline
set modelines=5

" some handy keybinds
nnoremap <silent> <leader>n :nohlsearch <cr>
nnoremap <silent> <leader>w :let _save_pos=getpos(".") <Bar>
    \ :let _s=@/ <Bar>
    \ :%s/\s\+$//e <Bar>
    \ :let @/=_s <Bar>
    \ :nohl <Bar>
    \ :unlet _s <Bar>
    \ :call setpos('.', _save_pos) <Bar>
    \ :unlet _save_pos <cr>

" vim-airline config
let g:airline_theme='solarized'
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#formatter = 'unique_tail_improved'

" fzf.vim config
let g:fzf_buffers_jump = 1
let g:fzf_preview_window = ['up:40%', 'ctrl-/']
nnoremap <silent> <leader>b :Buffers <cr>
nnoremap <silent> <leader>f :GFiles <cr>
